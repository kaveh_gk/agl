///<reference path:"../node_modules/@types/jquery/index.d.ts" />

$(function () {
    // Get json file
    $.getJSON('http://agl-developer-test.azurewebsites.net/people.json', function(data){
        
        var maleCatName = [];
        var femaleCatName = [];
        // Going through the main data fetched from json file
        $(data).each(function(idx, val){
            // Check if the owner is Male
            if(val.gender == "Male") {
                $(val.pets).each(function(index, item){
                    $(item).each(function(i, childPets){
                        // Check if the pet type is Cat
                        if(childPets.type == "Cat") {
                            maleCatName.push(childPets.name);
                            maleCatName = maleCatName.sort();
                        }
                    }); 
                });
            } 
            // If the owner is Female
            else {
                $(val.pets).each(function(index, item){
                    $(item).each(function(i, childPets){
                        if(childPets.type == "Cat") {
                            // Check if the pet type is Cat
                            femaleCatName.push(childPets.name);
                            femaleCatName = femaleCatName.sort();
                        }
                    }); 
                });
            }
        });

        // Iterating Male Column  -----------------------------------------------------------------------------------------------------------
        $(maleCatName).each(function(i, val){
            var sortedMaleCats = '<li class="list-group-item">'+ maleCatName[i] +'</li>';
            $('.maleOwner .js_catsName').append(sortedMaleCats);
        });

        // Iterating Female Column  -----------------------------------------------------------------------------------------------------------
        $(femaleCatName).each(function(i, val){
            var sortedMaleCats = '<li class="list-group-item">'+ femaleCatName[i] +'</li>';
            $('.femaleOwner .js_catsName').append(sortedMaleCats);
        });
    });
});